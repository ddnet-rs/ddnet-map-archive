#!/bin/bash

mkdir -p sha256
git -C ../ddnet-maps rev-list --all --objects | rg '.map$' | awk '{ print $1 }' | sort | uniq |
	while read line
	do
		echo next: $line
		git -C ../ddnet-maps cat-file blob $line > "sha256/$(git -C ../ddnet-maps cat-file blob $line | sha256sum | awk '{ print $1 }').map"
	done
